import {useState,useEffect} from 'react';
import {Link} from "react-router-dom"
// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

    const {name,description,price,_id} = course;


return (
 <Row className="mt-3 mb-3">
  <Col xs={12}>
   <Card className="cardHighlight p-0">
    <Card.Body>
     <Card.Title><h4>{name}</h4></Card.Title>
     <Card.Subtitle>Description</Card.Subtitle>
     <Card.Text>{description}</Card.Text>
     <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details </Button>
    </Card.Body>
   </Card>
  </Col>
 </Row>        
    )
}
// [S50 ACTIVITY END]




























/*import {Card,Row,Col,Button} from "react-bootstrap"



export default function CourseCard() {
  return (
      <Row className="mt-3 mb-3">
          <Col xs={12}>
              <Card className="courseCard p-3">
                  <Card.Body>
                      <Card.Title>
                          <h2>Sample Course</h2>
                      </Card.Title>
                      <Card.Text>
                          <div>Description:</div>
                          <p>This is a sample offering</p>
                          <div> Price: </div>
                          <div>Php 40,000 </div>
                      </Card.Text>
                      <Button variant="primary">Enroll</Button>
                  </Card.Body>
              </Card>
          </Col>
      </Row>
  )
}*/