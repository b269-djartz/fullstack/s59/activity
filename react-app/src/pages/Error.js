import Banner from "../components/Banner"
import { Link } from "react-router-dom"

export default function Error() {
  return (
    <>
      <Banner
        title="Error"
        description="Page not found."
        buttonText="Go back to homepage"
        buttonLink="/"
      />
    </>
  );
}
