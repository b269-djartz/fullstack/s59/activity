import {useState, useEffect,useContext} from 'react';

import { Form, Button } from 'react-bootstrap';
import {Navigate,useNavigate,Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function Register() {
    const {user, setUser} = useContext(UserContext)

    // to store and manage value of the input fields
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate('')

    useEffect(() => {
        if(( email !== "" && password1 !== "" && password2 !=="" && firstname !=="" && lastname !=="" && phoneNumber !=="") && password1 === password2) {
            setIsActive(true);
        } else {
            setIsActive(false);
        };
    }, [email, password1, password2]);


    function checkPhoneNumber(number) {
      if (number.length > 11) {
        alert("Number must only be 11 digits");
        return number.slice(0, 11); 
      } else {
        return number;
      }
    }

    // function to simulate user registration
    function registerUser(e) {
        e.preventDefault();

        if(phoneNumber.length < 11) {
            return alert("Phone number must be 11 digits")
        }
        // Clear input fields
        
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email

            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);

            // If no user information is found, the "access" property will not be available and will return undefined
            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
            if(data === true) {
                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                alert("Email Already exists")
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstname,
                lastName: lastname,
                mobileNo: phoneNumber,
                email: email,
                password: password1

            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);

            // If no user information is found, the "access" property will not be available and will return undefined
            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
            if(data === true) {
                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                setEmail("");
                setPassword1("");
                setPassword2("");
                setFirstname("");
                setLastname("");
                setPhoneNumber("");

                alert("Thank you for registering!");
                navigate("/login")
            } else {
                return alert("error")
            }
    });
            }
    });


    };

    return (
        (user.id !== null)? 
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)} >
            
            <Form.Group controlId="firstname">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter firstname" 
                    value={firstname}
                    onChange={e => setFirstname(e.target.value)}
                  required
                />
            </Form.Group>

            <Form.Group controlId="lastname">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter lastname" 
                    value={lastname}
                    onChange={e => setLastname(e.target.value)}
                  required
                />
            </Form.Group>

            <Form.Group controlId="phoneNumber">
                <Form.Label> Phone number</Form.Label>
                <Form.Control 
                  type="number" 
                  placeholder="Enter phone number" 
                    value={phoneNumber}
                    onChange={e => (setPhoneNumber(checkPhoneNumber(e.target.value)))}
                  required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                  type="email" 
                  placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                  required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                  required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                  required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
            
        </Form>
    )

}


