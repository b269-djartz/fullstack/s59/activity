/*import coursesData from "../data/coursesData"*/
import {useState, useEffect} from 'react';
import CourseCard from "../components/CourseCard";

export default function Courses() {
	// console.log(coursesData)
	// return (
	// 	<> 
	// 	   {coursesData.map((course) => (
    //     <CourseCard key={course.id} course={course} />
    //   ))}
	// 	</>

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

	return (
		<>
		{courses}
		</>
	) 
}

